# Git du Projet Tutoré S2 -- Projet Bomberman

*Voix de Ivan* Bonjour et bienvenue dans ce projet 

**Fonctionne sur Windows, Linux et MacOSX**


**Attention** pour faire fonctionner ce projet sur votre machine il faut le module **pyqt5** :


pour l'installer :

`pip3 install pyqt5`

**ou**

`python(3) -m pip install pyqt5`

---


**Fonctions en essai actuellement :**
- Playlist Musicale (Midnight Man [Extended Mix] - Flash & The Pan / Hot In The City - Billy Idol / Hot Water [MasterMix] - Level 42 / Love Is Like Oxygen - Sweet / Shooting Stars - Bag Raiders / Two Tribes - Frankie Goes To Hollywood / Is This Love - Whitesnake) *d'autres musiques seront rajoutées dans le futur (merci de me faire part de vos propositions :smile: )*
- Explosion de Bombes (**fonctionnent à 75%**)
- Game Menu plus sympa que l'actuel

TODO : finir ce readme